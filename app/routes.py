from flask import Blueprint
from app.models import User


main = Blueprint("main", __name__)


@main.route('/')
@main.route('/index')
def index():
    return "Hello, World!"
