from app import db
from flask_login import UserMixin
from werkzeug.security import generate_password_hash, check_password_hash


class User(UserMixin, db.Model):
    id = db.Column(db.Integer, primary_key=True)
    email = db.Column(db.String(100), index=True, unique=True)
    password_hash = db.Column(db.String(128))

    @property
    def password(self):
        raise AttributeError("password is not a readable attribute")

    @password.setter
    def password(self, password):
        self.password_hash = generate_password_hash(password)

    def check_password(self, password):
        return check_password_hash(self.password_hash, password)

    @staticmethod
    def create_admin_user(email, password):
        user = db.session.query(User).filter_by(email=email).one_or_none()
        if user is None:
            u = User(email=email, password=password)
            db.session.add(u)
            db.session.commit()

    def __repr__(self):
        return "<User %r>" % self.email
