from flask import Blueprint, render_template, redirect, url_for, request, flash, url_for
from flask_login import current_user, login_user, logout_user
from app.models import User
from app import db
auth = Blueprint("auth", __name__)


@auth.route("/login", methods=["GET", "POST"])
def login():
    if request.method == "GET":
        if current_user.is_authenticated:
            return redirect(url_for("main.index"))
    if request.method == "POST":
        email = request.form["email"]
        password = request.form["password"]

        remember_me = False
        if "remember_me" in request.form:
            remember_me = True
        registered_user = db.session.query(User).filter_by(email=email).first()
        if registered_user is not None and registered_user.check_password(password):
            login_user(registered_user, remember=remember_me)
            flash("User successfully logged in")
            return redirect(url_for("main.index"))
        flash("Username or password are invalid")
        return redirect(url_for("auth.login"))

    return render_template("login.html")


@auth.route("/logout")
def logout():
    if current_user.is_authenticated:
        logout_user()
        flash("User logged out")

    return redirect(url_for("main.index"))
